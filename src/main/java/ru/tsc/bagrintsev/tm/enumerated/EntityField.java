package ru.tsc.bagrintsev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum EntityField {
    ID("id"),
    INDEX("index"),
    NAME("name"),
    DESCRIPTION("description"),
    STATUS("status"),
    SORT("sort type"),
    PROJECT_ID("project id"),
    TASK_ID("task id"),
    LOGIN("login"),
    PASSWORD("password"),
    EMAIL("email"),
    FIRST_NAME("first name"),
    MIDDLE_NAME("middle name"),
    LAST_NAME("last name"),
    LOCKED("locked"),
    ROLE("role"),
    USER_ID("user id"),
    COMMAND_NAME("name"),
    COMMAND_SHORT("short name");

    @NotNull
    private final String displayName;

    EntityField(@NotNull final String displayName) {
        this.displayName = displayName;
    }

}
