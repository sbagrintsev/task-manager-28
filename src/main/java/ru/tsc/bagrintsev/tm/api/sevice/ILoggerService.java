package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.Nullable;

public interface ILoggerService extends Checkable {

    void info(@Nullable final String message);

    void command(@Nullable final String message);

    void debug(@Nullable final String message);

    void error(@Nullable final Exception e);

}
