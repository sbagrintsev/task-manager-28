package ru.tsc.bagrintsev.tm.api.model;

public interface IWBS extends IHasName, IHasStatus, IHasDateCreated, IHasDateStarted {
}
