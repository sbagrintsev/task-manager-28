package ru.tsc.bagrintsev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.LOGIN);
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final String userId = getUserService().findByLogin(login).getId();
        showParameterInfo(EntityField.PASSWORD);
        @NotNull final String password = TerminalUtil.nextLine();
        try {
            getUserService().setPassword(userId, password);
        } catch (IdIsEmptyException e) {
            throw new UserNotFoundException();
        }
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getName() {
        return "user-change-password";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change user password.";
    }

}
