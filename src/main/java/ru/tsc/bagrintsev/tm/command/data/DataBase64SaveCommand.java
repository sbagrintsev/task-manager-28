package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Base64;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        try (@NotNull final ByteArrayOutputStream baos = new ByteArrayOutputStream();
             @NotNull final ObjectOutputStream oos = new ObjectOutputStream(baos);
             @NotNull final FileOutputStream fos = new FileOutputStream(file, false)) {
            oos.writeObject(domain);
            @NotNull final byte[] bytes = baos.toByteArray();
            @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
            fos.write(base64.getBytes());
            fos.flush();
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-base64";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in base64 file";
    }

}
