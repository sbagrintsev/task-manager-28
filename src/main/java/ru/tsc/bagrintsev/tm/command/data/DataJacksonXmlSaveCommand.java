package ru.tsc.bagrintsev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import java.io.FileOutputStream;

public final class DataJacksonXmlSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        try (@NotNull final FileOutputStream fos = new FileOutputStream(FILE_JACKSON_XML)) {
            fos.write(xml.getBytes());
            fos.flush();
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in xml file";
    }

}
