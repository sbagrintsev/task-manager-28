package ru.tsc.bagrintsev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.dto.Domain;
import ru.tsc.bagrintsev.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JACKSON_XML = "./data.jackson.xml";

    @NotNull
    public static final String FILE_JACKSON_JSON = "./data.jackson.json";

    @NotNull
    public static final String FILE_JACKSON_YAML = "./data.jackson.yaml";

    @NotNull
    public static final String FILE_JAXB_XML = "./data.jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data.jaxb.json";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (serviceLocator == null) return domain;
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null || serviceLocator == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getAuthService().signOut();
    }

    @Override
    public @NotNull String getShortName() {
        return "";
    }

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
