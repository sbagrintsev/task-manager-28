package ru.tsc.bagrintsev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project by id.";
    }

}
